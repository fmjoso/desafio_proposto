package model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries(
		{@NamedQuery(name=Tasks.FIND_ALL_TASKS, query="select t from Tasks t where t.solved <> 1")}
		)
public class Tasks implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	public static final String FIND_ALL_TASKS = "Tasks.findAllTasks";
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE) @Column(name="task_id")
	private long taskId;
	@Column(name = "task_date")
	@Temporal(TemporalType.DATE)
	private Date taskDate;
	@Column(name = "description")
	private String description;
	@Column(name = "answer")
	private String answer;
	@ManyToOne
	@JoinColumn(name = "owner_id")
	private Users owner;
	@ManyToOne
	@JoinColumn(name = "target_id")
	private Users targetUser;
	@Column(name="solved")
	private Boolean solved;
	
	
	public Tasks() {
		
	}


	public long getTaskId() {
		return taskId;
	}


	public void setTaskId(long taskId) {
		this.taskId = taskId;
	}


	public Date getTaskDate() {
		return taskDate;
	}


	public void setTaskDate(Date taskDate) {
		this.taskDate = taskDate;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getAnswer() {
		return answer;
	}


	public void setAnswer(String answer) {
		this.answer = answer;
	}


	public Users getOwner() {
		return owner;
	}


	public void setOwner(Users owner) {
		this.owner = owner;
	}


	public Users getTargetUser() {
		return targetUser;
	}


	public void setTargetUser(Users targetUser) {
		this.targetUser = targetUser;
	}
	
	public Boolean getSolved() {
		return solved;
	}

	public void setSolved(Boolean solved) {
		this.solved = solved;
	}


	@Override
	public String toString() {
		return "Tasks [taskId=" + taskId + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (taskId ^ (taskId >>> 32));
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tasks other = (Tasks) obj;
		if (taskId != other.taskId)
			return false;
		return true;
	}
	
	
}
