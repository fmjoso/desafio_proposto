package model;

import java.io.Serializable;
import java.util.ArrayList;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

@Entity
@NamedQueries(
		{@NamedQuery(name=Users.FIND_USER_LOGIN, query="select u from Users u where u.login = :login and u.password = :password")}
		)
public class Users implements Serializable {
	
	private static final long serialVersionUID = 1L;
	public static final String FIND_USER_LOGIN = "Users.findUserLogin";
	
	
	@Id @GeneratedValue(strategy=GenerationType.SEQUENCE) @Column(name="user_id")
	private long userId;
	@Column(name="login", nullable = false)
	private String login;
	@Column(name="user_name", nullable = false)
	private String userName;
	@Column(name="password", nullable = false)
	private String password;
	
	@OneToMany(mappedBy="owner", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private ArrayList<Tasks> ownerTasks;
	
	@OneToMany(mappedBy="targetUser", fetch=FetchType.LAZY, cascade=CascadeType.REMOVE)
	private ArrayList<Tasks> toDoTasks;
	
	public Users(){
		
	}

	public long getUserId() {
		return userId;
	}

	public void setUserId(long userId) {
		this.userId = userId;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Users [userId=" + userId + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (userId ^ (userId >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Users other = (Users) obj;
		if (userId != other.userId)
			return false;
		return true;
	}
	
	
}
