package services;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import model.Tasks;

@Stateless
public class TaskService {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	public  List<Tasks> getAllTasks(){
		try{
			List<Tasks> tasks = entityManager.createNamedQuery(Tasks.FIND_ALL_TASKS, Tasks.class)
					.getResultList();
			return tasks;
		} catch (Exception e) {
			return null;
		}
	}
	
	//salva uma nova task
	public void saveNewTask(Tasks t){
		entityManager.persist(t);
	}
	
	//atualiza tasks de acordo com as requisições do bean
	public void updateTask(Tasks t){
		entityManager.merge(t);
	}

}
