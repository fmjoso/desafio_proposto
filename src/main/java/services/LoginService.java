package services;


import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import model.Users;


@Stateless
public class LoginService {
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	//verifica se já existe o admin caso não, ele cria
	public Boolean existAdmin(){
		String findAdmin = "select u from Users u where u.login = :login";
		try {
			Users admin = entityManager.createQuery(findAdmin, Users.class)
					.setParameter("login", "admin")
					.getSingleResult();
			if(admin!=null){
				return true;
			}
			return false;
		} catch (Exception e) {
			return false;
		}
		
	}
	
	//insere o primeiro usuário no banco.
	public void insertFirstUser(){
		Users firstUser = new Users();
		firstUser.setLogin("admin");
		firstUser.setPassword("1234");
		firstUser.setUserName("Josué Machado");
		entityManager.persist(firstUser);
	}
	
	//consulta usuario via namedquery
	public Users autentication(String login, String password){
		try {
			Users u = entityManager.createNamedQuery(Users.FIND_USER_LOGIN, Users.class)
					.setParameter("login", login)
					.setParameter("password", password)
					.getSingleResult();
			return u;
		} catch (Exception e) {
			return null;
		}
		
	}
}
