package mbean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import org.primefaces.context.RequestContext;

import model.Tasks;
import services.TaskService;
import session.AccessSession;
import utils.Messenger;


@ManagedBean
@ViewScoped
public class TasksMB implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	
	@Inject
	private TaskService taskService;
	
	private Messenger msg;
	private AccessSession accessSession;
	private List<Tasks> listOfTasks = new ArrayList<Tasks>();
	private String description;
	private Tasks selectTask;
	private String answer;
	
	//getters and setters
	public AccessSession getAccessSession() {
		return accessSession;
	}

	public void setAccessSession(AccessSession accessSession) {
		this.accessSession = accessSession;
	}

	public List<Tasks> getListOfTasks() {
		return listOfTasks;
	}

	public void setListOfTasks(List<Tasks> listOfTasks) {
		this.listOfTasks = listOfTasks;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Tasks getSelectTask() {
		return selectTask;
	}

	public void setSelectTask(Tasks selectTask) {
		this.selectTask = selectTask;
	}

	public String getAnswer() {
		return answer;
	}

	public void setAnswer(String answer) {
		this.answer = answer;
	}
	//getters and setters fim

	//pega a lista de tasks e inicia o acesso a sessao para pegar user logado
	@PostConstruct
	public void init(){
		this.accessSession = new AccessSession();
		listOfTasks = taskService.getAllTasks();
	}
	
	public void newTask(){
		RequestContext.getCurrentInstance().execute("PF('dialogTask').show();");
	}
	
	public void cancelTask(){
		setDescription("");
	}
	
	//salva nova tarefa
	public void saveTask(){
		Tasks newTask = new Tasks();
		newTask.setDescription(this.getDescription());
		newTask.setSolved(false);
		newTask.setOwner(this.accessSession.getUserSession());
		newTask.setTaskDate(new Date());
		taskService.saveNewTask(newTask);
		RequestContext.getCurrentInstance().execute("PF('dialogTask').hide();");
		listOfTasks = taskService.getAllTasks();
		setDescription("");
		
	}
	
	public void openTaskSelected(){
		System.out.println("ehiueaeiu"+selectTask.getDescription());
		RequestContext.getCurrentInstance().execute("PF('dialogEditTask').show();");

	}

	public void unselectTask(){
		RequestContext.getCurrentInstance().execute("PF('dialogEditTask').hide();");
	}
	
	//atribui tarefa para si
	public void getTask(){
		this.selectTask.setTargetUser(accessSession.getUserSession());
		taskService.updateTask(selectTask);
	}
	
	//deixa tarefa livre
	public void leaveTask(){
		this.selectTask.setTargetUser(null);
		taskService.updateTask(selectTask);
	}
	
	//completa tarefa, esta não fica mais disponivel a outros usuarios
	public void completeTask(){
		msg = new Messenger();
		//verifica se o campo é required via código para não bloquear o action dos outros botões
		if(this.answer.equals("")){
			String m = msg.getMessageProperties("answeObligate");
			msg.doMessage(FacesMessage.SEVERITY_ERROR, m, "generic");
		} else {
			this.selectTask.setAnswer(getAnswer());
			this.selectTask.setSolved(true);
			taskService.updateTask(selectTask);
			setAnswer("");
			setSelectTask(null);
			RequestContext.getCurrentInstance().execute("PF('dialogEditTask').hide();");
			listOfTasks = taskService.getAllTasks();
			String m = msg.getMessageProperties("taskComplete");
			msg.doMessage(FacesMessage.SEVERITY_INFO, m, "editTask");
		}
	}
	
	//fecha modal e atualiza lista de tarefas para setar novos donos
	public void closeTaskEdit(){
		RequestContext.getCurrentInstance().execute("PF('dialogEditTask').hide();");
		listOfTasks = taskService.getAllTasks();
	}
	
	
	//mata a sessao e redireciona para a tela de login
	public String logout(){
		accessSession.killSession();
		return "/index?faces-redirect=true";
	}

}
