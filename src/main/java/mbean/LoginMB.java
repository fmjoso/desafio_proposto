package mbean;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Inject;
import javax.servlet.http.HttpSession;

import utils.Messenger;
import model.Users;
import services.LoginService;
import session.AccessSession;

@ManagedBean
@ViewScoped
public class LoginMB implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@Inject
	private LoginService loginService;
	
	private String login;
	private String password;
	//classe para simplificar mensagens de aviso
	private Messenger msg;
	
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@PostConstruct
	public void init(){
	}
	
	public String login() {
		msg = new Messenger();
		if ((this.getPassword().equals("")) && (this.getLogin().equals(""))) {
			String m = msg.getMessageProperties("loginCamposObrigatorios");
			msg.doMessage(FacesMessage.SEVERITY_ERROR, m, "errorLogin");
		}

		else if ((this.getPassword().equals("")) && (!this.getLogin().equals(""))) {
			String m = msg.getMessageProperties("loginInfoSenha");
			msg.doMessage(FacesMessage.SEVERITY_ERROR, m, "errorLogin");

		}

		else if ((!this.getPassword().equals("")) && (this.getLogin().equals(""))) {
			String m = msg.getMessageProperties("loginInfoEmail");
			msg.doMessage(FacesMessage.SEVERITY_ERROR, m, "errorLogin");

		}
		else {
			//como não há registro no banco quando iniciar a tela de login ele criará o banco e o user admin senha 1234
			//verifica se existe o admin caso não exista cria
			if(!loginService.existAdmin()){
				loginService.insertFirstUser();
			}
			Users u = new Users();
			u = loginService.autentication(this.getLogin(), this.getPassword());
			if (u == null) {
				String m = msg.getMessageProperties("loginInvalido");
				msg.doMessage(FacesMessage.SEVERITY_ERROR, m, "errorLogin");

			}
			else {
				return this.loginComplete(u);
			}
		}
		return("");
		
	}
	
	//caso login ok, adiciona o usuario na sessao criada e segue para página seguinte
	private String loginComplete(Users u) {
		AccessSession accessSession = new AccessSession();
		if(accessSession.getSession().getAttribute("user") != null){
			accessSession.getSession().removeAttribute("user");
		} else {
			accessSession.getSession().setAttribute("user", u);
		}
		return "/pages/tasks?faces-redirect=true";

	}

}
