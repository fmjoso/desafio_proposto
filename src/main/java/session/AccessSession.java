package session;

import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import model.Users;

public class AccessSession {
	
	private HttpSession session;
	private Users userSession;
	
	//cria a sessao e pega o usuario
	public AccessSession(){
		this.session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext()
				.getSession(false);
		this.userSession = (Users) session.getAttribute("user");
	}
	
	//finaliza a sessao
	public void killSession(){
		this.session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
		this.session.invalidate();
	}

	//getters and setters
	
	public HttpSession getSession() {
		return session;
	}

	public void setSession(HttpSession session) {
		this.session = session;
	}

	public Users getUserSession() {
		return userSession;
	}

	public void setUserSession(Users userSession) {
		this.userSession = userSession;
	}
	
	
}
