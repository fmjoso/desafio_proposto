package session;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

//é preciso adicionar o filtro criado no arquivo web.xml
public class Session implements Filter{
	
	public  Session(){
		
	}

	public void destroy() {
		// TODO Auto-generated method stub
		
	}
	
	//cria filtro para bloquear acesso a de páginas diretamente
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
		HttpSession session = (HttpSession) req.getSession(false);
		if(session == null || session.getAttribute("user") == null){
			res.sendRedirect(req.getContextPath()+"/index.xhtml");
		}
		else{
			chain.doFilter(request, response);
		}
		
	}

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

}
