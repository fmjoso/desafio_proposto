package utils;

import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.application.FacesMessage.Severity;
import javax.faces.context.FacesContext;

//classe generica para construir mensagem de um modo mais fácil
public class Messenger {
	
	private FacesContext facesContext;
	private FacesMessage msg;
	
	public Messenger(){
		//necessita isntanciar a cada função para pegar o contexto
		facesContext = FacesContext.getCurrentInstance();
	}
	
	public void doMessage(Severity severity, String message, String key){
		msg = new FacesMessage(severity, message, key);
		facesContext.addMessage(key, msg);
	}
	
	public String getMessageProperties(String key){
		ResourceBundle bundle = this.facesContext.getApplication().getResourceBundle(this.facesContext, "msg");
		String message = bundle.getString(key);
		return message;
	}

}
