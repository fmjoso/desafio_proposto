# README #

#TECNOLOGIAS EMPREGADAS
JavaEE, JSF, Primefaces, CDI, MySQL, EclipseLink, JTA, Git, WildFly, HTML, CSS

#INSTALAÇÃO
* 1-Instalação do servidor
*  1.1- Em seu Eclipse Jee Neon acesse Help->Eclipse MarketPlace
*   1.1.1- Instale o FreeMarker IDE From JBoss Tools 1.5 em seu eclipse
*  1.2- Baixe e descompacte o Wildfly já configurado(drives do eclipseLink e mysql já instalados e conexão com o banco de dados já criada) via link https://www.dropbox.com/s/1qjx92sddv8zn3r/wildfly-jta-desafio.rar?dl=0
*  1.3- Instale o servidor em seu eclipse
*   1.3.1- Window->Show View->Other... busque por Servers
*   1.3.2- Clique com o direito na aba Servers New->Server
*   1.3.3- Selecione JBoss Community wildfly 10.x e clique em Next e depois em Next novamente
*   1.3.4- Informe um nome qualquer para seu servidor
*   1.3.5- Em Home Directory selecione a pasta descompactada de seu WildFly baixado
*   1.3.6- Clique em next e logo após em confirm
*  1.4- Crie uma base de dados MySQL com os seguintes dados:
*        user:novo
*        senha:1234
*        base de dados: desafiodb (as tabelas serão criadas automaticamente no primeiro acesso)
*        OBS: você pode baixar o dump dessa base via https://www.dropbox.com/s/mi7a62xjihvtej7/Dumpdesafio.sql?dl=0
*        OBS: os valores de user e senha da conexão são importantes.

* 2- Baixe e faça o deploy da Aplicação 
*  2.1- Baixe e descompacte a aplicação via link https://bitbucket.org/fmjoso/desafio_proposto/downloads/
*  2.2- No Eclipse Jee Neon clique em file->import e procure por maven
*  2.3- Selecione Existing Maven Projects
*  2.4- Busque pela aplicação descompactada
*  2.5- selcione a checkbox que mostra um arquivo pom.xml e clique em finish
*  2.6- Já no Eclipse Jee Neon clique com o direito sobre o projeto Maven->Update Project
*       OBS: caso os erros não sumam de um Clean em seu projeto
*  2.7- Gere o .war: clique com o direito sobre o projeto Run As-> Maven Build... e digite 
clean install
*  2.8- Adicione o projeto no wildfly
*   2.8.1- Clique com o direito sobre o wildfly em seu eclipse Add and Remove e selecione o 
projeto
*   2.8.2- Start the server
*  2.9- Em seu navegador acesse http://localhost:8080/desafio/
*       Login: admin 
*       Senha: 1234
*       OBS: caso você não tenha importado o dump ele criará esse usuário para você

#CONTACT
email: jfurlanmachado@gmail.com